package com.company.airbeat.airbeat.Controller;

import android.content.Intent;
import android.os.AsyncTask;

import com.company.airbeat.airbeat.RegisterActivity;

public class RegisterUser extends AsyncTask<String,String,String> {

    //Attributes
    private UserInfoController userInfoController;

    //Constructor
    public RegisterUser(UserInfoController userInfoController){
        this.userInfoController = userInfoController;
    }

    @Override
    protected String doInBackground(String... params) {
        this.userInfoController.registerUser();
        return "";
    }
}
