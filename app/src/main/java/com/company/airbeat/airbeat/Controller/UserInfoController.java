package com.company.airbeat.airbeat.Controller;


import com.company.airbeat.airbeat.Model.UserInfoModel;

public class UserInfoController {
    //Attributes
    private String userID;
    private String firstName;
    private String lastName;
    private String email;
    private String userPassword;
    private UserInfoModel userInfoModel;

    //Constructor
    public UserInfoController(){
        this.userInfoModel = new UserInfoModel();
    }

    //Setters
    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    //Getters
    public String getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getUserPassword() {
        return userPassword;
    }


    //Methods
    public void registerUser(){
        this.userInfoModel.registerUser(this);
    }

    public boolean emailExists(){
        return this.userInfoModel.emailExisits(this);
    }

    public boolean loginUser(){
        return this.userInfoModel.loginUser(this);
    }

    public String getFullName(){
        return this.userInfoModel.getFullName(this);
    }
}
