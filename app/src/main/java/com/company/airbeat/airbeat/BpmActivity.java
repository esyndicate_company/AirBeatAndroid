package com.company.airbeat.airbeat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.company.airbeat.airbeat.BPM.HeartRateMonitor;
import com.company.airbeat.airbeat.BPM.ImageProcessing;

import java.util.concurrent.atomic.AtomicBoolean;

public class BpmActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private MenuItem nav_logout_menu;
    private MenuItem nav_basic_info_menu;
    private MenuItem nav_bpm_menu;
    private MenuItem nav_symptoms_menu;
    private MenuItem nav_charts_menu;
    private MenuItem nav_home_menu;
    private MenuItem nav_trigger_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;


    //---------------------------- Handlers ------------------------------------------
    Handler setNavigationHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setNavigationViewListner();
        }
    };
    Handler setSharedPreferenceValuesHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setSharedPreferenceValues();
        }
    };
    //----------------------------End of Handlers ------------------------------------
    //----------------------------End of navigation items----------------------------

    //----------------------------BPM Variables---------------------------------------

    private static final String TAG = "HeartRateMonitor";
    private static final AtomicBoolean processing = new AtomicBoolean(false);

    private static SurfaceView preview = null;
    private static SurfaceHolder previewHolder = null;
    private static Camera camera = null;
    private static View image = null;
    private static TextView text = null;
    int count = 0;

    private static PowerManager.WakeLock wakeLock = null;

    private static int averageIndex = 0;
    private static final int averageArraySize = 4;
    private static final int[] averageArray = new int[averageArraySize];

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.CAMERA
    };

    public static enum TYPE {
        GREEN, RED
    };

    private static HeartRateMonitor.TYPE currentType = HeartRateMonitor.TYPE.GREEN;

    public static HeartRateMonitor.TYPE getCurrent() {
        return currentType;
    }

    private static int beatsIndex = 0;
    private static final int beatsArraySize = 3;
    private static final int[] beatsArray = new int[beatsArraySize];
    private static double beats = 0;
    private static long startTime = 0;

    //----------------------------End of BPM Variables---------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bpm);

        //----------------------------Navigation components----------------------------
        setNavigationHandler.sendEmptyMessage(0);

        drawerLayout = (DrawerLayout) findViewById(R.id.bpm_page);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_basic_info_menu= menuNav.findItem(R.id.basic_info_menu);
        nav_bpm_menu= menuNav.findItem(R.id.bpm_menu);
        nav_symptoms_menu= menuNav.findItem(R.id.symptoms_menu);
        nav_charts_menu= menuNav.findItem(R.id.charts_menu);
        nav_trigger_menu= menuNav.findItem(R.id.trigger_menu);
        nav_home_menu= menuNav.findItem(R.id.home_menu);;


        setSharedPreferenceValuesHandler.sendEmptyMessage(0);

        //----------------------------End of navigation components----------------------------
        preview = (SurfaceView) findViewById(R.id.preview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    1
            );
        }

        image = findViewById(R.id.image);
        text = (TextView) findViewById(R.id.text);
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");



    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";


        // email = sharedPreferences.getString("email","");
        email="testing";
        //fullname = sharedPreferences.getString("full_name","");
        fullname = "testing";





        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                homeFunction();
                break;
            }

            case R.id.basic_info_menu: {
                //Calling the basic info function
                basicInfoFunction();
                break;
            }

            case R.id.bpm_menu: {
                //Calling the bpm function
                bpmFunction();
                break;
            }

            case R.id.symptoms_menu: {
                //Calling the symptoms function
                symptomsFunction();
                break;
            }

            case R.id.charts_menu:{
                //Calling the charts function
                chartsFunction();
                break;
            }

            case R.id.trigger_menu:{
                //Calling the my trigger function
                triggersFunction();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //the home function for the navigation
    public void homeFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bsic info function for the navigation
    public void basicInfoFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,BasicInfoActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bpm function for the navigation
    public void bpmFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,BpmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the symptoms function for the navigation
    public void symptomsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,SymptomsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the charts function for the navigation
    public void chartsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,ChartsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the triggers function for the navigation
    public void triggersFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,TriggerActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }


    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(BpmActivity.this,LoginActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();

        wakeLock.acquire();

        camera = Camera.open();

        startTime = System.currentTimeMillis();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        super.onPause();

        wakeLock.release();

        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {

        /**
         * {@inheritDoc}
         */
        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) throw new NullPointerException();
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) throw new NullPointerException();

            if (!processing.compareAndSet(false, true)) return;

            int width = size.width;
            int height = size.height;

            int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);
            // Log.i(TAG, "imgAvg="+imgAvg);
            if (imgAvg == 0 || imgAvg == 255) {
                processing.set(false);
                return;
            }

            int averageArrayAvg = 0;
            int averageArrayCnt = 0;
            for (int i = 0; i < averageArray.length; i++) {
                if (averageArray[i] > 0) {
                    averageArrayAvg += averageArray[i];
                    averageArrayCnt++;
                }
            }

            int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
            HeartRateMonitor.TYPE newType = currentType;
            if (imgAvg < rollingAverage) {
                newType = HeartRateMonitor.TYPE.RED;
                if (newType != currentType) {
                    beats++;
                    // Log.d(TAG, "BEAT!! beats="+beats);
                }
            } else if (imgAvg > rollingAverage) {
                newType = HeartRateMonitor.TYPE.GREEN;
            }

            if (averageIndex == averageArraySize) averageIndex = 0;
            averageArray[averageIndex] = imgAvg;
            averageIndex++;

            // Transitioned from one state to another to the same
            if (newType != currentType) {
                currentType = newType;
                image.postInvalidate();
            }

            long endTime = System.currentTimeMillis();
            double totalTimeInSecs = (endTime - startTime) / 1000d;
            int avgbeats = 0;
            if (totalTimeInSecs >= 10) {
                double bps = (beats / totalTimeInSecs);
                int dpm = (int) (bps * 60d);
                if (dpm < 30 || dpm > 180) {
                    startTime = System.currentTimeMillis();
                    beats = 0;
                    processing.set(false);
                    return;
                }

                // Log.d(TAG,
                // "totalTimeInSecs="+totalTimeInSecs+" beats="+beats);

                if (beatsIndex == beatsArraySize) beatsIndex = 0;
                beatsArray[beatsIndex] = dpm;
                beatsIndex++;

                int beatsArrayAvg = 0;
                int beatsArrayCnt = 0;
                for (int i = 0; i < beatsArray.length; i++) {
                    if (beatsArray[i] > 0) {
                        beatsArrayAvg += beatsArray[i];
                        beatsArrayCnt++;
                    }
                }
                int beatsAvg = (beatsArrayAvg / beatsArrayCnt);
                text.setText(String.valueOf(beatsAvg));
                avgbeats = beatsAvg;
                startTime = System.currentTimeMillis();
                beats = 0;
                count++;
            }

            if(count >= 4){
                startNewActivity();
            }

            processing.set(false);
        }
    };

    public void startNewActivity(){
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("PreviewsurfaceCallback", "Exception in setPreviewDisplay()", t);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d(TAG, "Using width=" + size.width + " height=" + size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }

        return result;
    }

}
