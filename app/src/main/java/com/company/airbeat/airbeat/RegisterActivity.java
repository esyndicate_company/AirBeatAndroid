package com.company.airbeat.airbeat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.RegisterUser;
import com.company.airbeat.airbeat.Controller.UserInfoController;

import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView , firstNameView, lastNameView , confirmPasswordView;
    private View mProgressView;
    private View mRegisterFormView;
    private TextView app_name_textview;
    private Button register_btn , sign_in_btn;

    //Declaring and creating the handler method
    Handler registerHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            register();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        firstNameView = (EditText) findViewById(R.id.first_name);
        lastNameView = (EditText) findViewById(R.id.last_name);
        confirmPasswordView = (EditText) findViewById(R.id.confirm_password);
        app_name_textview = (TextView) findViewById(R.id.app_name_text);
        register_btn = (Button) findViewById(R.id.register_button);
        sign_in_btn = (Button) findViewById(R.id.email_sign_in_button);
        mRegisterFormView = findViewById(R.id.register_scroll_form);
        mProgressView = findViewById(R.id.register_progress);


        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable registerRunnableTask = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this){
                            try{
                                registerHandler.sendEmptyMessage(0);
                            }catch (Exception ex){
                                System.out.println("Error in register button : "+ex);
                            }
                        }
                    }
                };
                //Creating the thread to be called the runnable task
                Thread registerThread = new Thread(registerRunnableTask);
                registerThread.start();
            }
        });

        sign_in_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable sign_in_runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this){
                            try{
                                //Setting the intent to redirect the page
                                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                //Redirecting the page
                                startActivity(intent);
                                finish();
                            }catch (Exception ex){
                                System.out.println("Error in sign in button in register page : "+ex);
                            }
                        }
                    }
                };
                //Creating the thread to be called the runnable task
                Thread signInThread = new Thread(sign_in_runnable);
                signInThread.start();
            }
        });

    }


    public void register(){
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        confirmPasswordView.setError(null);
        firstNameView.setError(null);
        lastNameView.setError(null);


        // Store values at the time of the login attempt.
        String email_vlaue = mEmailView.getText().toString();
        String password_value = mPasswordView.getText().toString();
        String confirm_password_value = confirmPasswordView.getText().toString();
        String first_name_value = firstNameView.getText().toString();
        String last_name_value = lastNameView.getText().toString();

        UserInfoController userInfoController = new UserInfoController();
        userInfoController.setFirstName(first_name_value);
        userInfoController.setLastName(last_name_value);
        userInfoController.setEmail(email_vlaue);
        userInfoController.setUserPassword(password_value);

        boolean cancel = false;
        View focusView = null;

        // Check if the field is empty
        if (TextUtils.isEmpty(email_vlaue)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }else if (!isEmailValid(email_vlaue)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if(userInfoController.emailExists()){
            mEmailView.setError(getString(R.string.error_email_exists));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password_value)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(confirm_password_value)) {
            confirmPasswordView.setError(getString(R.string.error_field_required));
            focusView = confirmPasswordView;
            cancel = true;
        } else if (!(confirm_password_value.equals(password_value))){
            confirmPasswordView.setError(getString(R.string.error_confirm_password));
            focusView = confirmPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(last_name_value)) {
            lastNameView.setError(getString(R.string.error_field_required));
            focusView = lastNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(first_name_value)) {
            firstNameView.setError(getString(R.string.error_field_required));
            focusView = firstNameView;
            cancel = true;
        }




        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            //Register function
            RegisterUser registerUser = new RegisterUser(userInfoController);
            registerUser.execute();

            Toast.makeText(this, "Successfully Registered", Toast.LENGTH_LONG).show();

            //Setting the intent to redirect the page
            Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
            //Redirecting the page
            startActivity(intent);
            finish();
        }

    }

    private boolean isEmailValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        return pat.matcher(email).matches();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
