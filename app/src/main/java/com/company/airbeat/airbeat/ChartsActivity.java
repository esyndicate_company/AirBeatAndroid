package com.company.airbeat.airbeat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ChartsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private MenuItem nav_logout_menu;
    private MenuItem nav_basic_info_menu;
    private MenuItem nav_bpm_menu;
    private MenuItem nav_symptoms_menu;
    private MenuItem nav_charts_menu;
    private MenuItem nav_home_menu;
    private MenuItem nav_trigger_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;


    //---------------------------- Handlers ------------------------------------------
    Handler setNavigationHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setNavigationViewListner();
        }
    };
    Handler setSharedPreferenceValuesHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setSharedPreferenceValues();
        }
    };
    //----------------------------End of Handlers ------------------------------------
    //----------------------------End of navigation items----------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts);

        //----------------------------Navigation components----------------------------
        setNavigationHandler.sendEmptyMessage(0);

        drawerLayout = (DrawerLayout) findViewById(R.id.charts_page);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_basic_info_menu= menuNav.findItem(R.id.basic_info_menu);
        nav_bpm_menu= menuNav.findItem(R.id.bpm_menu);
        nav_symptoms_menu= menuNav.findItem(R.id.symptoms_menu);
        nav_charts_menu= menuNav.findItem(R.id.charts_menu);
        nav_trigger_menu= menuNav.findItem(R.id.trigger_menu);
        nav_home_menu= menuNav.findItem(R.id.home_menu);;


        setSharedPreferenceValuesHandler.sendEmptyMessage(0);

        //----------------------------End of navigation components----------------------------
    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";


        // email = sharedPreferences.getString("email","");
        email="testing";
        //fullname = sharedPreferences.getString("full_name","");
        fullname = "testing";





        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                homeFunction();
                break;
            }

            case R.id.basic_info_menu: {
                //Calling the basic info function
                basicInfoFunction();
                break;
            }

            case R.id.bpm_menu: {
                //Calling the bpm function
                bpmFunction();
                break;
            }

            case R.id.symptoms_menu: {
                //Calling the symptoms function
                symptomsFunction();
                break;
            }

            case R.id.charts_menu:{
                //Calling the charts function
                chartsFunction();
                break;
            }

            case R.id.trigger_menu:{
                //Calling the my trigger function
                triggersFunction();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //the home function for the navigation
    public void homeFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bsic info function for the navigation
    public void basicInfoFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,BasicInfoActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bpm function for the navigation
    public void bpmFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,BpmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the symptoms function for the navigation
    public void symptomsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,SymptomsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the charts function for the navigation
    public void chartsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,ChartsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the triggers function for the navigation
    public void triggersFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,TriggerActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }


    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(ChartsActivity.this,LoginActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------
}
