package com.company.airbeat.airbeat.Utils;

public class ScriptPaths {
    public static final String BASE_IP = "http://192.168.1.5/";
    public static final String REGISTER_URL = BASE_IP + "AirBeatFiles/registerUser.php";
    public static final String LOGIN_URL = BASE_IP + "AirBeatFiles/loginUser.php";
    public static final String EMAIL_EXISTS = BASE_IP + "AirBeatFiles/emailExists.php";
    public static final String GET_FULLNAME = BASE_IP + "AirBeatFiles/getFullName.php";
}
